#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"

int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.txt", "r"); //otwieramy plik w trybie odczytu

    // tworzymy tablice dynamiczne, do ktorych wpisywane beda dane
    float *iksy;
    iksy = (float *)malloc(50 * sizeof(float));

    float *igreki;
    igreki = (float *)malloc(50 * sizeof(float));

    float *erhao;
    erhao = (float *)malloc(50 * sizeof(float));

    char tmp[4] = "";

    for(int i = 0; i<4; i++)
    {
        fscanf(plik, "%s", tmp);
    }

    for(int i = 0; i <50; i++) // pobieramy z pliku do tablic dane
    {
        fscanf(plik, "%s", tmp);
        fscanf(plik, "%f", &iksy[i]);
        fscanf(plik, "%f", &igreki[i]);
        fscanf(plik, "%f", &erhao[i]);
    }

    fclose(plik); // zamykamy plik
    plik = fopen("P0001_attr.txt", "a"); // otwieramy plik w trybie dopisywania

    fprintf(plik ,"\n");


    // dopisujemy dane do pliku
    sortowanie(iksy);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(iksy));

    sortowanie(igreki);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(igreki));

    sortowanie(erhao);
    fprintf(plik, "Mediana: %.2f", f_mediana(erhao));

    fprintf(plik ,"\n");

    fprintf(plik,"Srednia: %.3f\t\t", srednia(iksy));
    fprintf(plik,"Srednia: %.3f\t\t", srednia(igreki));
    fprintf(plik,"Srednia: %.3f", srednia(erhao));

    fprintf(plik ,"\n");

    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(iksy));
    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(igreki));
    fprintf(plik,"Odchylenie: %.3f", odch_stand(erhao));

    fclose(plik); // zamykamy plik

    return 0;
}

