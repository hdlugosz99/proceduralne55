#include <stdio.h>
#include "fun.h"

void sortowanie(float *tab){
	float tmp = 0.0;
    int rozmiar = 50;

	for(int i=0; i<rozmiar; i++){
		for(int j=1; j<rozmiar-i; j++){
            if(*(tab + j-1)>*(tab + j)){
                tmp = *(tab + j-1);
                *(tab + j-1) = *(tab + j);
                *(tab + j) = tmp;
            }
        }
    }

    return;
}
