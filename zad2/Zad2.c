#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fun.h"


struct dane //struktura zawierajaca dane z pliku
{
    float iksy[50];
    float igreki[50];
    float erhao[50];

}dane;

struct wyniki // struktura zawierajaca wyniki obliczen
{
    float srednia, mediana, odchylenie;
}wyniki1[3];

int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.txt", "r"); // otwiramy plik w trybie odczytu

    char tmp[4] = "";

    for(int i = 0; i<4; i++)
    {
        fscanf(plik, "%s", tmp);
    }

    // pobieramy dane z pliku i wpisujemy do struktur
    for(int i = 0; i <50; i++)
    {
        fscanf(plik, "%s", tmp);
        fscanf(plik, "%f", &dane.iksy[i]);
        fscanf(plik, "%f", &dane.igreki[i]);
        fscanf(plik, "%f", &dane.erhao[i]);
    }

    int find_result = 0;
    char temp[512];

    // wyszukujemy slowa "Mediana:" w pliku, jesli wystepuje, oznacza to ze dane sa juz wpisane
    while(fgets(temp, 512, plik) != NULL)
    {
		if((strstr(temp, "Mediana:")) != NULL)
        {
			find_result++;
		}
	}

	fclose(plik); // zamykamy plik

    if(find_result == 0) // dane nie sa wpisane, wpisujemy je
    {
        plik = fopen("P0001_attr.txt", "a"); // otwieramy plik w trybie dopisywania

        fprintf(plik ,"\n");

        // wpisujemy wyniki do pliku
        sortowanie(dane.iksy);
        wyniki1[1].mediana = f_mediana(dane.iksy);
        fprintf(plik, "Mediana: %.2f\t\t", f_mediana(dane.iksy));

        sortowanie(dane.igreki);
        wyniki1[2].mediana = f_mediana(dane.igreki);
        fprintf(plik, "Mediana: %.2f\t\t", f_mediana(dane.igreki));

        sortowanie(dane.erhao);
        wyniki1[3].mediana = f_mediana(dane.erhao);
        fprintf(plik, "Mediana: %.2f", f_mediana(dane.erhao));

        fprintf(plik ,"\n");

        wyniki1[1].srednia = srednia(dane.iksy);
        fprintf(plik,"Srednia: %.3f\t\t", srednia(dane.iksy));
        wyniki1[2].srednia = srednia(dane.igreki);
        fprintf(plik,"Srednia: %.3f\t\t", srednia(dane.igreki));
        wyniki1[3].srednia = srednia(dane.erhao);
        fprintf(plik,"Srednia: %.3f", srednia(dane.erhao));

        fprintf(plik ,"\n");
        wyniki1[1].odchylenie = odch_stand(dane.iksy);
        fprintf(plik,"Odchylenie: %.3f\t", odch_stand(dane.iksy));
        wyniki1[2].odchylenie = odch_stand(dane.igreki);
        fprintf(plik,"Odchylenie: %.3f\t", odch_stand(dane.igreki));
        wyniki1[3].odchylenie = odch_stand(dane.erhao);
        fprintf(plik,"Odchylenie: %.3f", odch_stand(dane.erhao));

        fclose(plik); // zamykamy plik
   }
   else // dane sa juz wpisane, pojawia sie komunikat
        printf("Wyniki sa juz wpisane!\n");

    return 0;
}
