#include <stdio.h>
#include "fun.h"

float f_mediana(float *tab){  //deklaracja i definicja funkcji obliczajacej mediane
    float mediana = 0.0;
    float tmp = 0.0;
    int rozmiar = 50;

    if(rozmiar % 2 == 0){
       tmp = *(tab + (rozmiar/2)-1) + *(tab + (rozmiar/2));
       mediana = tmp/2;
       }
    else if(rozmiar % 2 != 0){
       mediana = *(tab + (rozmiar/2));
       }

return mediana;
}
