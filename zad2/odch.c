#include <stdio.h>
#include "fun.h"

float odch_stand(float *tab){
    float tmp = 0.0;
    float wart_sr = srednia(tab);

    for(int i=0; i<50; i++)
        tmp += powf(tab[i] - wart_sr, 2);

float wart_odch_stand = sqrtf(tmp/50);
return wart_odch_stand;
}
